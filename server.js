const express = require('express');
const bodyParser = require('body-parser');
var path = require('path');
var favicon = require('serve-favicon');
var routes = require('./routes');
var logger = require('morgan');
var compress = require('compression');
var port = process.env.PORT || 4000;

var { InitGraphQL , Schema } = require ('./graphqlIntegration');

const MongoClient = require('mongodb').MongoClient;
const MONGO_URL = 'mongodb://localhost:27017/suventure';

var app = express(),
  dir = __dirname,
  publicDir = path.join(dir, 'public'),
  imagesDir = path.join(dir, 'public', 'images'),
  buildDir = path.join(dir, 'public', 'build');


var db;

// CORS
app.use(logger('dev'));
app.use(compress());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.use(express.static(publicDir));
app.use(bodyParser.raw({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '50mb'
}));
app.use(bodyParser.json());

app.use(favicon(path.join(imagesDir, 'favicon.ico')));

var schema;

MongoClient.connect(MONGO_URL, (err, database) => {
  if (err) {
    app.get ('/', (req, res, next) => {
      res.sendFile(path.join(publicDir, 'index.html'));
    });

    app.use((req, res, next) => {
      res.sendFile(path.join(publicDir, 'index.html'));
    });

    app.listen(port, () => {
      console.log('Error occured while connecting to DB but listening on', port)
    });
    return console.log(err)
  }
  else {
    db = database;
    InitGraphQL (db);
    schema = Schema;

    app.get ('/', (req, res, next) => {
      res.sendFile(path.join(publicDir, 'index.html'));
    });

    app.get('/api/products',
            (req, res) => routes.getData(req, res, db, schema));

    app.get('/api/product/:productId',
            (req, res) => routes.getProductById(req, res, db, schema));

    app.delete('/api/product/:productId',
            (req, res) => routes.deleteProductById(req, res, db, schema));

    app.post('/api/product',
            (req, res) => routes.addProduct(req, res, db, schema));

    app.put('/api/product',
            (req, res) => routes.updateProduct(req, res, db, schema));

    app.use((req, res, next) => {
      res.sendFile(path.join(publicDir, 'index.html'));
    });

    app.listen(port, () => {
      console.log('listening on', port)
    });
  }
});
