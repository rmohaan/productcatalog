/*global document alert*/

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import * as actions from '../actions/index';
import { Link } from 'react-router';
import * as utils from '../utils';

class ViewProduct extends React.Component {

constructor ()  {
  super();
  this.goBack = () => this._goBack();
  this.deleteProduct = (item) => this._deleteProduct(item);
  this.editProduct = (item) => this._editProduct(item);
}

_goBack () {
  this.props.dispatch(push('/'))
}

_deleteProduct (item) {
  this.props.dispatch(actions.deleteProduct(item));
}

_editProduct (item) {
  this.props.dispatch(actions.editProduct(item));
}

render () {
    var item = this.props.product,
        rupeeSign = '&#x20B9',
        stockStatus = item.availability > 0 ? utils.inStock : utils.outOfStock,
        tagValue = item.availability > 0 ? utils.tagSuccess : utils.tagDanger,
        srcImage = item.url ? item.url : utils.noImageUrl;

    return (
      <div className="container-fluid">
        <h1 className="page-header text-align-center">{item.name}</h1>
        <div className="container">
          <div className="col-md-3 img-holder">
            <img src={srcImage} width="200" height="360" />
          </div>
          <div className="col-md-6">
            <div> {item.brand} </div>
            <br />
            <div> {item.description} </div>
            <br />
            <div className={`tag ${tagValue}`}>{stockStatus}</div>
            <br />
            <div className="boldText"> <span className="rupee-sign">&#x20B9;</span> {item.price} </div>
          </div>
        </div>
        <div>
          <hr/>
          <div>
            <button className="btn btn-default" onClick={this.goBack}>Back</button>
            <span className="float-right">
              <Link to={`/edit/${item._id}`} className="btn btn-primary">Edit Product</Link>
              <button className="btn btn-danger adjacentSpace" onClick={() => {if(confirm('Delete the Product?')) this.deleteProduct(item)}}>Delete Product</button>
            </span>
          </div>
        </div>
      </div>
    );
  }
}

function select (state) {
  return {
    product: state.product
  };
}

export default connect(select)(ViewProduct);
