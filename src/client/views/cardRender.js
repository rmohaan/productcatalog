/*global document alert*/

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import { Link } from 'react-router';
import * as utils from '../utils';

class CardRender extends React.Component {

  constructor ()  {
    super();
    this.generateProductLayout = (list, cols) => this._generateProductLayout(list, cols);
    this.generateNoDataFound = () =>  this._generateNoDataFound();
  }

  _generateProductLayout (list, cols) {
    let defaultImage = utils.noImageUrl;
    return list.map((item, index) => {
      var img = cols.map((colData, index) => {
          if (colData == 'url'){
            let imgSrc = item[colData] ? item[colData] : defaultImage;
            return (
              <div key={index}>
                <img style={{width: '150px', height: '350px'}}
                      src={`${imgSrc}`} />
               <div className="priceTag"> Rs. {item['price']} </div>
              <hr className="hr-styling"/>
              </div>);
          }
      });
      var cells = cols.map((colData, index) => {
        if (colData != 'url' && colData != 'price' && colData != '_id'){
          if (colData == 'name') {
            return (<Link key={index} to={`/view/${item._id}`} className="font-remove-underline boldText">{item[colData]}</Link>);
          } else if (colData == 'availability') {
            let data = item[colData],
                stockStatus = data > 0 ? utils.inStock : utils.outOfStock,
                tagValue = data > 0 ? utils.tagSuccess : utils.tagDanger;
            return (<p key={index} className={`tag ${tagValue}`}> {stockStatus} </p> );
          }
          return (<p key={index}> {item[colData]} </p> );
        }
      });
      return (
        <div className="col-md-6" key={index}>
          <div className="col-md-4" key={index}> {img}  </div>
          <div className="col-md-8" key={index+1}>
            {cells}
            <Link to={`/view/${item._id}`}>View More</Link>
          </div>
        </div>
      );
    });
}

_generateNoDataFound () {
  return <div> No Data Found </div>;
}

render () {
    let list = this.props.productList,
        cols = this.props.cols,
        header = '';
    if (this.props.productList) {
      list = this.generateProductLayout(list, cols);
    }
    else {
      list = this.generateNoDataFound();
    }
    return (
      <div className="cardRender">
        {list}
      </div>
    );
  }
}

export default CardRender;
