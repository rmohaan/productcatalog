/*global location*/

'use strict';

import axios from 'axios';

axios.interceptors.response.use((response) => {
  return response;
}, (error) => {
  if (error.response !== undefined && error.response.status === 401) {
    location.reload();
  }
  return Promise.reject(error);
});

export function fetchProducts () {
  return axios({
    method: 'get',
    url: '/api/products'
  });
}

export function getProduct (productId) {
  return axios({
    method: 'get',
    url: `/api/product/${productId}`
  });
}

export function deleteProduct (productItem) {
  console.log(productItem);
  return axios({
    method: 'delete',
    url: `/api/product/${productItem._id}`,

  });
}

export function addProduct (productItem) {
  console.log(productItem);
  return axios({
    method: 'post',
    url: '/api/product',
    data: productItem
  });
}

export function updateProduct (productItem) {
  console.log(productItem);
  return axios({
    method: 'put',
    url: '/api/product',
    data: productItem
  });
}

export function submitData(items) {
  return axios({
    method: 'put',
    url: '/api/submitData',
    data: items
  });
}

export function fetchProductsList () {
  return axios.all([
    fetchProducts()
  ])
  .then(axios.spread(function (responseData) {
    // ... but this callback will be executed only when both requests are complete.
    return {
      data: responseData.data
    };
  }));
}
