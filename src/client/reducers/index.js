'use strict';

import * as actionEvents from '../actions/events';

export function productList (state, action) {
  let actionType = action.type,
      newState = Object.assign({}, state);

  if (actionType === actionEvents.SET_PRODUCTS_LIST) {
    newState = action.payload;
  }

  return newState;
}

export function product (state, action) {
  let actionType = action.type,
      newState = Object.assign({}, state);

  if (actionType === actionEvents.SET_PRODUCT_DATA) {
    newState = action.payload;
  }

  return newState;
}

export function currentProduct (state, action) {
  let actionType = action.type,
      newState = Object.assign({}, state);

  if (actionType === actionEvents.SET_CURRENT_PRODUCT_DATA) {
    newState = action.payload;
  }

  return newState;
}
