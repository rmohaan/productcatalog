'use strict';

export const noImageUrl = 'http://vignette1.wikia.nocookie.net/bokunoheroacademia/images/d/d5/NoPicAvailable.png/revision/latest?cb=20160326222204';
export const inStock = 'In Stock';
export const outOfStock = 'Out of Stock';
export const tagSuccess = 'tag-success';
export const tagDanger = 'tag-danger';
