/*global document alert*/

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import Header from './header';
import Footer from './footer';
import HomePage from './homePage';
import * as actions from '../actions/index';
import { connect } from 'react-redux';

class Home extends React.Component {

  componentDidMount () {
    this.props.dispatch(actions.fetchProductsList());
  }

  render () {
      return (
        <div className="container-fluid">
          <Header />
          <div className="container-fluid">
            <h1 className="page-header text-align-center">Sample Product Catalog
              <span className="float-right"><a className="btn btn-success" href="/add"> Add Product</a></span>
            </h1>
            <HomePage />
          </div>
          <Footer />
        </div>
      );
    }
}

export default connect()(Home);
