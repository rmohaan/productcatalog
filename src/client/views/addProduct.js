/*global document alert*/

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import Header from './header';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import * as actions from '../actions/index';
import Select from 'react-select';

const options = [
    { value: 'electronics', label: 'Electronics' },
    { value: 'clothing', label: 'Clothing' },
    { value: 'others', label: 'Others' }
],
defaultOption = options[0];

class AddProduct extends React.Component {

constructor ()  {
  super();
  this.state = {
    name: '',
    description: '',
    availability: '',
    price: '',
    url: '',
    category: '',
    errorMessage: ''
  }
  this.goBack = () => this._goBack();
  this.nameChanged = (event) => this._nameChanged(event);
  this.descriptionChanged = (event) => this._descriptionChanged(event);
  this.availabilityChanged = (event) => this._availabilityChanged(event);
  this.priceChanged = (event) => this._priceChanged(event);
  this.urlChanged = (event) => this._urlChanged(event);
  this.categoryChanged = (event) => this._categoryChanged(event);
  this.addProduct = () => this._addProduct();
  this.validateRequiredFields = () => this._validateRequiredFields();
  this.validateNumberInputs = () => this._validateNumberInputs();
  this.isValidNumber = (num) => this._isValidNumber(num);
  this.createProductModelFromState = () => this._createProductModelFromState();
}

_isValidNumber(num) {
  //num = parseFloat(num);
  return !isNaN(num) && isFinite(num);
}

_goBack () {
  this.props.dispatch(push('/'))
}

_createProductModelFromState () {
  var product = {
    name: this.state.name,
    description: this.state.description,
    availability: parseFloat(this.state.availability),
    price: parseFloat(this.state.price),
    url: this.state.url,
    category: this.state.category
  }
  return product;
}

_addProduct () {
  var errorMsg = '';
  if (this.validateRequiredFields()) {
    if(this.validateNumberInputs()) {
      var product = this.createProductModelFromState();
      console.log("Request for Add products");
      this.props.dispatch(actions.addProduct(product));
    } else {
      errorMsg = 'Availability and Price are numeric fields.';
    }
  }
  else {
    errorMsg = 'Please fill out required fields.';
  }
  this.setState({
    errorMessage: errorMsg
  })
}

_validateRequiredFields () {
  return this.state.name && this.state.price && this.state.category && this.state.availability;
}

_validateNumberInputs () {
  return this.isValidNumber(this.state.price) && this.isValidNumber(this.state.availability);
}

_nameChanged (event) {
  event.preventDefault();
  this.setState ({
    name: event.target.value
  });
}

_descriptionChanged (event) {
  event.preventDefault();
  this.setState ({
    description: event.target.value
  });
}

_availabilityChanged (event) {
  event.preventDefault();
  this.setState ({
    availability: event.target.value
  });
}

_priceChanged (event) {
  event.preventDefault();
  this.setState ({
    price: event.target.value
  });
}

_urlChanged (event) {
  event.preventDefault();
  this.setState ({
    url: event.target.value
  });
}

_categoryChanged (event) {
  this.setState ({
    category: event.value
  });
}

render () {

    var errorMessage = this.state.errorMessage;
    return (
      <div className="container-fluid">
      <Header />
        <h1 className="page-header" style={{textAlign: 'center'}}>Add New Product</h1>
        <div className="container">
          <div className="col-md-6">
            <div className="errorMessage">
              {errorMessage}
            </div>
            <form>
              <div className="form-group">
                <label htmlFor="name">Product Name*</label>
                <input type="text"
                       id="name"
                       className="form-control"
                       value={this.state.name}
                       onChange={this.nameChanged}
                       placeholder="Provide product name"
                       autoFocus />
              </div>
              <br />
              <div className="form-group">
                <label htmlFor="description">Product Description</label>
                <input type="text"
                       id="description"
                       className="form-control"
                       value={this.state.description}
                       onChange={this.descriptionChanged}
                       placeholder="Provide product description" />
              </div>
              <br />
              <div>
                <label htmlFor="availability">Product Availability*</label>
                <input type="text"
                       id="availability"
                       className="form-control"
                       value={this.state.availability}
                       onChange={this.availabilityChanged}
                       placeholder="Provide product availability/quantity"/>
              </div>
              <br />
              <div>
                <label htmlFor="price">Product Price*</label>
                <input type="text"
                           id="price"
                           className="form-control"
                           value={this.state.price}
                           onChange={this.priceChanged}
                           placeholder="Provide product price/cost"/>
              </div>
              <br />
              <div>
                <label htmlFor="url">Product Image Url</label>
                <input type="text"
                       id="url"
                       className="form-control"
                       value={this.state.url}
                       onChange={this.urlChanged}
                       placeholder="Provide product image url" />
              </div>
              <br />
              <div>
                <label htmlFor="url">Product Category*</label>
                <Select options={options}
                          onChange={this.categoryChanged}
                          value = {this.state.category}
                          placeholder="Select an option"
                          clearable={false} />
              </div>
            </form>
          </div>
        </div>
        <div>
          <hr/>
          <div>
            <button className="btn btn-primary" onClick={this.goBack}> Back </button>
            <span style={{float: 'right'}}>
              <button className="btn btn-success" onClick={() => {this.addProduct()}}> Add Product </button>
            </span>
          </div>
        </div>
      </div>
    );
  }
}

function select (state) {
  return {
    product: state.product
  };
}

export default connect(select)(AddProduct);
