var graphql = require('graphql');
var { makeExecutableSchema } = require('graphql-tools');
var ObjectId = require('mongodb').ObjectId;
var async = require('asyncawait/async');
var await = require('asyncawait/await');

const prepare = (o) => {
  o._id = o._id.toString();
  return o;
}

var Products;
var InitGraphQL = async ( function (db) {
  Products = db.collection('products');
});

var typeDefs = [`
    type Query {
      products: [Product],
      getProduct(_id: ID): Product
    }

    type Product {
      _id: ID,
      name: String,
      brand: String,
      description: String,
      availability: Float,
      price: Float,
      url: String,
      category: String
    }

    type Mutation {
      createProduct(name: String, brand: String, description: String, availability: Float, price: Float, url: String, category: String): Product,
      deleteProduct(_id: ID): Boolean,
      updateProduct(_id:ID, name: String, brand: String, description: String, availability: Float, price: Float, url: String, category: String): Product,
    }
    schema {
      query: Query,
      mutation: Mutation
    }
  `];

var resolvers = {
    Query: {
      products: async ( function () {
        return (await (Products.find({}).toArray()).map(prepare));
      }),
      getProduct: async ( function (_, args) {
        return prepare(await (Products.findOne(ObjectId(args._id))));
      }),
    },
    Mutation: {
      createProduct: async ( function (root, args, context, info) {
        const res = await (Products.insertOne(args));
        return prepare(await (Products.findOne({_id: res.insertedId})));
      }),
      deleteProduct: async ( function(_, args) {
        return await (Products.deleteOne({_id: ObjectId(args._id)}));
      }),
      updateProduct: async ( function(root, args, context, info) {
        var idToUpdate = args._id;
        delete args._id;
        const res = await (Products.update({_id: ObjectId(idToUpdate)}, args));
        return prepare(await (Products.findOne(ObjectId(idToUpdate))));
      })
    },
  }

var Schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

module.exports = {
  InitGraphQL,
  Schema
}
