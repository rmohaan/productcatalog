var ObjectId = require('mongodb').ObjectId;
var collectionName = 'products';
var { graphql } = require('graphql');

module.exports = {
  getData (req, res, db, schema) {
    graphql(schema, `{
      products {
        name
        description
        brand
        availability
        url
        price
        _id
      }
    }`)
      .then((results) => {
        res.status(200).json(results.data.products);
      });
  },

  getProductById (req, res, db, schema) {
    var id = req.params.productId;
    var o_id = new ObjectId(id);
    var query = `{
          getProduct (_id: "${id}") {
            name,
            description,
            brand,
            availability,
            url,
            price,
            category,
            _id
          }
        }`;
    graphql(schema, query)
      .then((results) => {
        res.status(200).json(results.data.getProduct);
      });
  },

  deleteProductById (req, res, db, schema) {
    var id = req.params.productId;
    var o_id = new ObjectId(id);
    var mutation = `mutation {
          deleteProduct (_id: "${id}")
        }`;
    graphql(schema, mutation)
      .then((results) => {
        console.log("result", results);
        res.status(200).json(results.data.deleteProduct);
      });
  },

  addProduct (req, res, db, schema) {
    var body = req.body;
    var mutation = `mutation {
          createProduct (
            name: "${body.name}",
            brand: "${body.name}",
            description: "${body.description}",
            availability: ${body.availability},
            price: ${body.price},
            url: "${body.url}",
            category: "${body.category}"
          ) {
            name,
            brand
            description,
            availability,
            price,
            url,
            category,
            _id
          }
        }`;
    graphql(schema, mutation)
      .then((results) => {
        res.status(200).json(results.data.createProduct);
      });
  },

  updateProduct (req, res, db, schema) {
    var body = req.body;
    var mutation = `mutation {
          updateProduct (
            _id: "${body.id}",
            name: "${body.name}",
            brand: "${body.name}",
            description: "${body.description}",
            availability: ${body.availability},
            price: ${body.price},
            url: "${body.url}",
            category: "${body.category}"
          ) {
            name,
            brand
            description,
            availability,
            price,
            url,
            category,
            _id
          }
        }`;
    graphql(schema, mutation)
      .then((results) => {
        res.status(200).json(results.data.updateProduct);
      });
  },

};
