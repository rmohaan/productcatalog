/*global document alert*/

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import * as actions from '../actions/index';
import { connect } from 'react-redux';
import CardRender from './cardRender';

class HomePage extends React.Component {

render () {
    let data = this.props.productList.data ? this.props.productList.data : [],
        cols = ['name', 'brand', 'description', 'url', 'price', '_id', 'availability'];

    return (
      <div>
        <CardRender productList={data} cols={cols}/>
      </div>
    );
  }
}

function select (state) {
  return {
    productList: state.productList
  };
}

export default connect(select)(HomePage);
