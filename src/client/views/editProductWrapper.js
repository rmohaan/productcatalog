/*global document alert*/

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import Header from './header';
import Footer from './footer';
import EditProduct from './editProduct';
import * as actions from '../actions/index';
import { connect } from 'react-redux';

class EditProductWrapper extends React.Component {

  componentDidMount () {
    console.log(this.props);
    this.props.dispatch(actions.fetchProduct(this.props.params.id));
  }

  render () {
      return (
        <div className="container-fluid">
          <Header />
          <div className="container-fluid">
            <EditProduct />
          </div>
          <Footer />
        </div>
      );
    }
}

export default connect()(EditProductWrapper);
