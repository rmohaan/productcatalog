/*global document alert*/

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

export default class Footer extends React.Component {

render () {
    return (
      <footer className="footer-left">
          <div className="container drag-left">
          <p className="text-footer">
            <a href='/'> suventure.in </a> &copy; 2016.  Sample Product Site.
          </p>
          </div>
     </footer>
    );
  }
}
