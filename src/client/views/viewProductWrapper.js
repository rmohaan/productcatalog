/*global document alert*/

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import Header from './header';
import Footer from './footer';
import ViewProduct from './viewProduct';
import * as actions from '../actions/index';
import { connect } from 'react-redux';

class ViewProductWrapper extends React.Component {
  constructor () {
    super();
    this.state = {
      links: ''
    };
    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
  }

  componentDidMount () {
    console.log(this.props);
    this.props.dispatch(actions.fetchProduct(this.props.params.id));
  }

  render () {
      return (
        <div className="container-fluid">
          <Header />
          <div className="container-fluid">
            <ViewProduct />
          </div>
          <Footer />
        </div>
      );
    }
}

export default connect()(ViewProductWrapper);
