'use strict';

import * as actionEvents from './events';
import * as dataRequests from './dataRequests';
import { push } from 'react-router-redux';


export function setData (data) {
  return {
    type: actionEvents.SET_DATA,
    payload: data
  };
}

export function setProduct (data) {
  return {
    type: actionEvents.SET_PRODUCT_DATA,
    payload: data
  };
}

export function setProductsList (data) {
  return {
    type: actionEvents.SET_PRODUCTS_LIST,
    payload: data
  };
}

export function editProduct (data) {
  return {
    type: actionEvents.SET_CURRENT_PRODUCT_DATA,
    payload: data
  };
}

export function fetchData () {
  return function (dispatch) {
    // dispatch(fetchingData());
    return dataRequests.fetchData()
       .then(function (response) {
         if (response.status === 200) {
           dispatch(setData(response.data));
         }
       })
       .catch((err) => {
         console.log(err);
          dispatch(push('/'))
       });
  };
}

export function fetchProduct (productId) {
  return function (dispatch) {
    return dataRequests.getProduct(productId)
      .then(function (response) {
        dispatch(setProduct(response.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export function deleteProduct (productItem) {
  return function (dispatch) {
    return dataRequests.deleteProduct(productItem)
      .then(function (response) {
        //console.log(response);
        dispatch(push('/'));
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export function addProduct (product) {
  return function (dispatch) {
    return dataRequests.addProduct(product)
      .then(function (response) {
        dispatch(push('/'));
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export function updateProduct (product) {
  return function (dispatch) {
    return dataRequests.updateProduct(product)
      .then(function (response) {
        dispatch(push('/'));
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export function fetchProductsList () {
  return function (dispatch) {
    // dispatch(fetchingData());
    return dataRequests.fetchProductsList()
       .then(function (response) {
         dispatch(setProductsList(response));
       })
       .catch((err) => {
         console.log(err);
          dispatch(push('/'))
       });
  };
}
